$( document ).ready(function() {
	// Navigation

	var navHamburger = $(".mobile-nav");
	var navContainer = $("#navigation ul");

	navHamburger.click(function(e){
		navContainer.fadeToggle( "fast", "linear" );
		e.stopPropagation();
	});

	navContainer.click(function(e){
		e.stopPropagation();
	});

	$(document).click(function(){
		navContainer.fadeOut();
	});

	// Slider

	$('.flexslider').flexslider({
		animation: "slide",
		rtl: true,
		directionNav: false,
		start: function(slider){
		  $('body').removeClass('loading');
		}
	});
});
